using System;
using System.Collections.Generic;

namespace Upwork_Test.Models.Table
{
    public class TableDocumnetColumnModel
    {
        public double ColumnWidthInPercent { get; set; }
        public String Header { get; set; }
        public List<Object> ColumnData { get; set; }

        public TableDocumnetColumnModel()
        {
            ColumnData = new List<object>();

        }

    }
}