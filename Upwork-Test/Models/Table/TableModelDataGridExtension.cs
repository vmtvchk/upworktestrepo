using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Upwork_Test.Models.Table
{
    public static class TableModelDataGridExtension
    {
        public static TableDocumenetModel BuildTableDocumnetModel(this DataGrid dataGrid)
        {
            if (dataGrid != null)
            {
                var datagridWidth =
                    dataGrid.Columns.Where(x => x.Visibility == Visibility.Visible).Sum(c => c.ActualWidth);//count total width of visible columns

                var tableModel = new TableDocumenetModel();

                for (int colIndex = 0; colIndex < dataGrid.Columns.Count; colIndex++)
                {
                    if (dataGrid.Columns[colIndex].Visibility == Visibility.Visible)
                    {
                        var column = dataGrid.Columns[colIndex];

                        TableDocumnetColumnModel columnModel = new TableDocumnetColumnModel();

                        columnModel.ColumnWidthInPercent = column.ActualWidth / datagridWidth;//for proportions
                        columnModel.Header = column.Header.ToString();

                        for (int i = 0; i < dataGrid.Items.Count; i++)
                        {
                            var data = dataGrid.GetCell(i, colIndex).GetCellValue();
                            columnModel.ColumnData.Add(data);
                        }

                        tableModel.Columns.Add(columnModel);
                    }

                }
                tableModel.Header = "HEADER!";
                return tableModel;
            }
            return null;
        }
    }
}