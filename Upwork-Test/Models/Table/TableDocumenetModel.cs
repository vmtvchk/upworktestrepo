﻿using System.Collections.Generic;

namespace Upwork_Test.Models.Table
{
    public class TableDocumenetModel
    {
        public string Header { get; set; }
        public List<TableDocumnetColumnModel> Columns { get; set; }

        public TableDocumenetModel()
        {
            Header = "";
            Columns = new List<TableDocumnetColumnModel>();
        }
    }
}
