﻿namespace Upwork_Test.Pdf.TextResolver
{
    public class ResolverGeneric<T,G>:Resolver
    {
        public ResolverGeneric() : base(typeof(T), typeof(G))
        {
        }
        
    }
}