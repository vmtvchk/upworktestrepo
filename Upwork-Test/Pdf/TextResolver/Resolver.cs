﻿using System;

namespace Upwork_Test.Pdf.TextResolver
{
    public abstract class Resolver : IResolver
    {
        protected Type _inputType;
        protected Type _outputType;

        public Resolver(Type inputType, Type outputType)
        {
            _inputType = inputType;
            _outputType = outputType;
        }

        public Type InputType
        {
            get { return _inputType; }

        }

        public Type OutputType
        {
            get { return _outputType; }
        }

        public virtual object Convert(object inp)
        {
            throw new NotImplementedException();
        }
    }
}
