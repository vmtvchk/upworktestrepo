﻿namespace Upwork_Test.Pdf.TextResolver
{
    public class BoolToStringResolver : ResolverGeneric<bool, string>
    {
        public override object Convert(object inp)
        {
            var data = (bool) inp;
            if (data) return "√";
            return "";

        }
    }
}