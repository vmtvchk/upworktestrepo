﻿using System;

namespace Upwork_Test.Pdf.TextResolver
{
    public interface IResolver
    {
        object Convert(Object inp);
        Type InputType { get; }
        Type OutputType { get; }
    }
}