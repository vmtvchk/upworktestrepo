using System.Collections.Generic;
using System.Linq;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using Upwork_Test.Models.Table;
using Upwork_Test.Pdf.TextResolver;

namespace Upwork_Test.Pdf.Editor
{
    public class DocumentEditor : IDocumentEditor
    {
        private Document _document;
        private readonly List<Resolver> _resolvers = new List<Resolver>();
        private Table _table;


        public DocumentEditor()
        {
            _resolvers.Add(new BoolToStringResolver());
        }

        public void InitDoc(string author, string title, string subject)
        {
            _document = new Document();
            _document.Info.Title = title;
            _document.Info.Subject = subject;
            _document.Info.Author = author;
            DefineStyles();
        }

        public void InsertTable(TableDocumenetModel model)
        {
            InitTable(model);
            BuildColumns(model);

            var rowsTotal = model.Columns.Max(x => x.ColumnData.Count);

            for (var rowIndex = 0; rowIndex < rowsTotal; rowIndex++)
            {
                var row1 = _table.AddRow();
                var row2 = _table.AddRow();

                for (var i = 0; i < model.Columns.Count; i++)
                    if (model.Columns[i].ColumnData.Count < rowIndex || model.Columns[i].ColumnData[rowIndex] == null)
                    {
                        row1.Cells[i].MergeDown = 1;
                        row1.Cells[i].AddParagraph("");
                    }
                    else
                    {
                        row1.Cells[i].MergeDown = 1;
                        var convertedData = TryToResolve(model.Columns[i].ColumnData[rowIndex]).ToString();

                        row1.Cells[i].AddParagraph(convertedData); //aditional conversion for some types
                    }

                _table.SetEdge(0, _table.Rows.Count - 2, model.Columns.Count, 2, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        public void Save(string filename)
        {
            var renderer = new PdfDocumentRenderer(true, PdfFontEmbedding.Always);
            renderer.Document = _document;
            renderer.RenderDocument();

            renderer.PdfDocument.Save(filename);
        }


        public void AddResolver(Resolver resolver)
        {
            _resolvers.Add(resolver);
        }

        public void RemoveResolver(Resolver resolver)
        {
            _resolvers.RemoveAll(x => x.InputType == resolver.InputType);
        }


        private void DefineStyles()
        {
            var style = _document.Styles["Normal"];
            style.Font.Name = "Verdana";

            style = _document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = _document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            style = _document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Verdana";
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = _document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }


        private void InitTable(TableDocumenetModel model)
        {
            var section = _document.AddSection();
            var paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Style = "Reference";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.AddFormattedText(model.Header, TextFormat.Bold);

            _table = section.AddTable();
            _table.Style = "Table";
            _table.Borders.Color = Colors.AliceBlue;
            _table.Borders.Width = 0.25;
            _table.Borders.Left.Width = 0.5;
            _table.Borders.Right.Width = 0.5;
            _table.Rows.LeftIndent = 0;
        }

        private void BuildColumns(TableDocumenetModel model)
        {
            var availeableSpace = _document.DefaultPageSetup.PageWidth -
                                  (_document.DefaultPageSetup.RightMargin + _document.DefaultPageSetup.LeftMargin);

            foreach (var tableDocumnetColumnModel in model.Columns)
            {
                Unit size = availeableSpace * tableDocumnetColumnModel.ColumnWidthInPercent;
                var column = _table.AddColumn(size);
                column.Format.Alignment = ParagraphAlignment.Center;
            }

            var row = _table.AddRow();

            for (var i = 0; i < model.Columns.Count; i++)
            {
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = Colors.LightGray;
                row.Cells[i].AddParagraph(model.Columns[i].Header);
            }
            _table.SetEdge(0, 0, model.Columns.Count, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        private object TryToResolve(object input)
        {
            var typeReolver = _resolvers.FirstOrDefault(x => x.InputType == input.GetType());

            if (typeReolver != null)
                return typeReolver.Convert(input);

            return input.ToString();
        }
    }
}