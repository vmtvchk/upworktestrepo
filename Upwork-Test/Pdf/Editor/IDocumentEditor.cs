﻿using System;
using Upwork_Test.Models.Table;
using Upwork_Test.Pdf.TextResolver;

namespace Upwork_Test.Pdf.Editor
{
    public interface IDocumentEditor
    {
        void AddResolver(Resolver resolver);
        void InitDoc(string author, string title, string subject);
        void InsertTable(TableDocumenetModel model);
        void RemoveResolver(Resolver resolver);
        void Save(String filename);
    }
}