﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Microsoft.Win32;
using Upwork_Test.Models.Table;
using Upwork_Test.Pdf.Editor;

namespace Upwork_Test.Behaviors
{
    public class PdfGeneratorBehavior : Behavior<DataGrid>
    {
        private readonly IDocumentEditor _editor;

        public PdfGeneratorBehavior()
        {
            ExportCommand = new RelayCommand(x => true, ExecuteExportCommand);
            _editor = new DocumentEditor();
        }

        public ICommand ExportCommand { get; set; }

        private void ExecuteExportCommand(object o)
        {
            if (AssociatedObject != null)
            {
                var dialog = new SaveFileDialog();
                dialog.DefaultExt = ".pdf";
                dialog.FileName = "SampleDoc";
                dialog.Filter = "Pdf Files|*.pdf";
                var result = dialog.ShowDialog();

                if (result.HasValue && result.Value)
                {
                    var tableModel = AssociatedObject.BuildTableDocumnetModel();
                    _editor.InitDoc("", "", "");
                    _editor.InsertTable(tableModel);
                    _editor.Save(dialog.FileName);
                }
            }
        }

    }
}