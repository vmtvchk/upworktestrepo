﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Upwork_Test
{
      
    public class ViewModel: INotifyPropertyChanged
    {
       
        public ObservableCollection<Text> Collection { get; set; } = new ObservableCollection<Text>();
      
        public ViewModel()
        {
            Collection.CollectionChanged += Collection_CollectionChanged;
        }
       
        public event PropertyChangedEventHandler PropertyChanged;

        private void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Collection"));
        }

        public void AddInitial()
        {
            for(int i = 0; i < 40;i++)
            Collection.Add(new Text() { Text1 = "Hello " + i.ToString(), Check = true, Cat = "Group1" });
        }

        public void Add10()
        {

            for (int i = 0; i < 10; i++)
                Collection.Add(new Text() { Text1 = "Added " + i.ToString(), Check = true, Cat = "Group2" });
        }

   
    }
}
