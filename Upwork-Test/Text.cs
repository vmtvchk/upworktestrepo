﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upwork_Test
{
    
    public class Text : INotifyPropertyChanged
    {
        public Text() {   }

        public string Text1
        {
            get { return _text1; }
            set
            {
                _text1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Text1"));
            }
        }
        private string _text1;

        public bool Check
        {
            get { return _check; }
            set
            {
                _check = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Check"));
            }
        }
        private bool _check;

        public string Cat
        {
            get { return _cat; }
            set
            {
                _cat = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cat"));
            }
        }
        private string _cat;


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
